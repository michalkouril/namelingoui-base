# docker build . --platform linux/x86_64 -t namelingoui-base
FROM rocker/shiny:4.3.2

# RENV_PATHS_ROOT=/srv/shiny-server/cache

RUN apt update && apt install -y python3-pip python3-venv python3-virtualenv

RUN R -e 'install.packages(c("shinyBS","ggplot2","jsonlite","MASS","yaml","reticulate"))'
RUN R -e 'install.packages(c("renv"))'
RUN R -e 'install.packages(c("DT"))'
RUN R -e 'install.packages(c("shinyjs"))'

USER shiny

RUN R -e 'reticulate::virtualenv_create(envname = "python3_env", python = "/usr/bin/python3")'
RUN R -e 'reticulate::virtualenv_install("python3_env", packages = c("deep-phonemizer==0.0.19"))'
RUN R -e 'reticulate::virtualenv_install("python3_env", packages = c("nltk"))'
RUN R -e 'reticulate::virtualenv_install("python3_env", packages = c("bs4"))'
RUN R -e 'reticulate::virtualenv_install("python3_env", packages = c("pysyllable"))'
RUN R -e 'reticulate::virtualenv_install("python3_env", packages = c("aquila-resolve"))'
RUN R -e 'reticulate::virtualenv_install("python3_env", packages = c("pluralizer"))'
RUN R -e 'reticulate::virtualenv_install("python3_env", packages = c("python-BLICK"))'

RUN R -e 'reticulate::use_virtualenv("python3_env", required = T)'
RUN R -e 'library(reticulate); use_virtualenv("python3_env", required = T); Aquila_Resolve <- import("Aquila_Resolve"); g2p <- Aquila_Resolve$G2p()'
# RUN R -e 'Aquila_Resolve <- reticulate::import("Aquila_Resolve")'
# ; g2p <- Aquila_Resolve$G2p() '
